# UI Testing Cypress

The project was automated inside the GitLab CI. <br>
The code is inside the Gitlab repository, and can be clone to local. <br>
Once push the updated code to the remote repository, the pipeline will be triggered automatically using Docker.<br>
After pipeline jobs are completed, the **'Test Report' 'screenshots'** and **'videos'** are generated as **job artifacts (Gitlab>CI/CD>Jobs)** <br><br>

(https://gitlab.com/AbdulkadirC/uitest)

(  )<br>

## How to install and run
- Clone the repository from Gitlab
- Install Docker
- cd into the project directory
- First, create a 'report' , screenshots' and 'videos' directories inside ''cypress" folder for reports to be placed in after the tests are done in docker file
For Mac and Linux

```
cd cypress && mkdir report screenshots videos && cd ..
``` 

For Windows Powershell

```
cd cypress; mkdir report,screenshots,videos; cd ..
``` 

- Build the docker image from the Dockerfile, you should be in the same directory with the Dockerfile for this command to work as expected:
```
docker build -t bynder-ui-test-img .
```   

- After build is completed, run the image by binding the target directory to the image:
```
docker run -it -v ${PWD}/cypress/report:/app/cypress/report -v ${PWD}/cypress/screenshots:/app/cypress/screenshots -v ${PWD}/cypress/videos:/app/cypress/videos bynder-ui-test-img
``` 

After the tests are done, the container will exit and hand over the CLI back. Then check the 'report' , 'screenshots' and 'videos' directories inside ''cypress" folder.
<br>



You can see the Login Page test Scenarios below;<br><br>

## Test Scenarios <br>

**Scenario:**  Should be able to login with valid credentials and then Logout <br>
&nbsp; **Given** As a user I am on the login page <br>
&nbsp; **When** Typing the email in the email field  <br>
&nbsp; **And** Typing the password in the password field <br>
&nbsp; **And** Clicking the login button <br>
&nbsp; **Then** Logining successfully and I redirected to the dashboard page <br>
&nbsp; **And** I submit to close the IE11 notification <br>
&nbsp; **Then** Click on the profile dropdown menu <br>
&nbsp; **And** Click on the logout button <br>
&nbsp; **Then** I should be redicreted to the login page <br>
&nbsp; **And** A message as 'You successfully logged out.' should be displayed  <br><br>

**Scenario:**  Should not be able to login with non-existing email and password <br>
&nbsp; **Given** As a user I am on the login page <br>
&nbsp; **When** Typing the email in the email field  <br>
&nbsp; **And** Typing the password in the password field <br>
&nbsp; **And** Clicking the login button <br>
&nbsp; **Then** A message of incorrect username or password should be displayed <br><br>

**Scenario:**  Should be able to see the language dropdown and language list <br>
&nbsp; **Given** As a user I am on the login page <br>
&nbsp; **When** Clicking the Language button  <br>
&nbsp; **Then** Should see the \<Language> option in the list <br>
``` {.sourceCode .gherkin}
|Language| 
|Dutch   | 
|English | 
|French  | 
|German  | 
|Italian | 
|Spanish | 
```


**Scenario Outline:**  Select Dutch language and check the Login Page translations<br> 
&nbsp; **Given** I am on the login page <br>
&nbsp; **When** I click the Language button  <br>
&nbsp; **And** I select a "\<Dutch>" from the dropdown list   <br>
&nbsp; **Then** The "\<LanguageBtn>" "\<EmailPlaceHolder>" "\<PasswordPlaceHolder>" "\<LostPasswordBtn>" "\<LoginBtn>" "\<SupportBtn>" should be translated <br>
``` {.sourceCode .gherkin}
Examples:
|Language|LanguageButton |EmailPlaceHolder          |PasswordPlaceHolder |LostPasswordButton                  |LoginButton|SupportBtn   | 
|Dutch   |Taal           |E-mail/Gebruikersnaam     |Wachtwoord          |Wachtwoord vergeten?                |Inloggen   |Ondersteuning| 

``` 

**Scenario:**  When user click Bynder logo then go to the Bynder main page <br>
&nbsp; **Given** As a user I am on the login page <br>
&nbsp; **When** Clicking the Bynder logo  <br>
&nbsp; **Then** Should be dicreted to the Bynder main page   <br><br>

**Scenario:**  Click Lost password? link and check the form and click Wave button <br>
&nbsp; **Given** I am on the login page <br>
&nbsp; **When** I click the Lost password? <br>
&nbsp; **Then** I should see the reset password form  <br>
&nbsp; **And** I click Wave button <br>
&nbsp; **Then** I should be redirected to login page   <br><br>

**Scenario Outline:** Login changing Language to French and check button translations <br>
&nbsp; **Given** I am on the login page <br>
&nbsp; **When** I click the Language button <br>
&nbsp; **And** I select a "\<Language>" from the dropdown list  <br>
&nbsp; **Then** I type the email in the email field  <br>
&nbsp; **And** I type the password in the password field <br>
&nbsp; **And** I click the login button <br>
&nbsp; **Then** I login successfully and I redirected to the dashboard page <br>
&nbsp; **And** I check the "\<Upload Media btn>"<br>
&nbsp; **Then** I click on the profile dropdown menu and check the "\<loginbtn>" translation <br>
&nbsp; **And** I click on the logout button <br>
&nbsp; **Then** I should be redicreted to the login page <br>
&nbsp; **And** I should see the "\<successfully logged out>" message in selected language <br>
``` {.sourceCode .gherkin}
Examples:
|Language|Upload Media btn |loginbtn   |successfully logged out                |
|French  |Importer         |Déconnexion|Vous vous êtes correctement déconnecté.|

```


