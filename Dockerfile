FROM cypress/browsers:node12.18.3-chrome87-ff82

WORKDIR /app
COPY package.json .
RUN yarn global add cypress
RUN yarn
COPY . .
CMD [ "npm", "run", "cypress"]