/// <reference types = "Cypress" />

class LoginPage{
    visitLoginPage(){
        cy.visit("https://wave-trial.getbynder.com/login/")
    }

    email(){
        return cy.get("#inputEmail")
    }

    password(){
        return cy.get('#inputPassword')
    }

    loginButton(){
        return cy.get(".login-btn")
    }

    profile(){
        return cy.get('.profile')
    }

    logoutButton(){
        return cy.get('form > .action-btn')
    }

    messageBox(){
        return cy.get('.cbox_messagebox')
    }

    languageButton(){
        return cy.get('.admin-options')
    }

    languageList(){
        return cy.get('a.admin-option')
    }

    lostPasswordLink(){
        return cy.get('.lost-password>a')
    }

    bynderLogoButton(){
        return cy.get('.admin-bar-logo')
    }

    supportLink(){
        return cy.get('.text')
    }

    emailForgotPassword(){
        return cy.get('#forgotPassword')
    }
    captchaForgotPassword(){
        return cy.get('img.captcha')
    }

    sendPasswordForgotLink(){
        return cy.get('#sendPassword')
    }

    waveLogoButton(){
        return cy.get('.account-logo')
    }

    captchaInputForgotPassword(){
        return cy.get('.required')
    }

    mediaUploadButton(){
        return cy.get('.cta-main')
    }

    captchaWordRobot(){
        return cy.get('.captcha')
    }


}

export default LoginPage;