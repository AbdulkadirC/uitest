
import LoginPage from "../../support/pageObjects/LoginPage";

describe("Login and Logout activities", ()=>{

    const login = new LoginPage()
    beforeEach(function(){
        login.visitLoginPage()
    })

    it("Should be able to login with valid credentials and then Logout",() => {
        login.email().type(Cypress.env('username'))
        login.password().type(Cypress.env('password'))
        login.loginButton().click()
        cy.url().should('eq','https://wave-trial.getbynder.com/account/dashboard/')
        login.profile().click()
        login.logoutButton().click()
        cy.url().should('eq', 'https://wave-trial.getbynder.com/login/')
        login.messageBox().should('have.text', 'You have successfully logged out.')
    });

    it("Should not be able to login with non-existing email and password",() => {
        login.email().type(Cypress.env('invalidUsername'))
        login.password().type(Cypress.env('invalidPassword'))
        login.loginButton().click()
        cy.url().should('not.eq', 'https://wave-trial.getbynder.com/account/dashboard/')
        
    });

    it("Should be able to see the language dropdown and language list",() => {
        //When the language list is as default in English (as a sample)
        login.languageButton().click()
        login.languageList(0).eq(0, 'Dutch (Netherlands)')
        login.languageList(1).eq(1, 'English (United States)')
        login.languageList(2).eq(2, 'French (France)')
        login.languageList(3).eq(3, 'German (Germany)')
        login.languageList(4).eq(4, 'Italian (Italy)')
        login.languageList(5).eq(5, 'Spanish (Spain)')

    });

    it("Select Dutch language and check the Login Page translations",() => {
        //When the Dutch language is selected as a sample
        login.languageButton().click()
        login.languageList(0).eq(0, 'Dutch (Netherlands)').click({force:true})
        login.languageButton().should('contains.text', 'Taal')
        login.email().should('have.attr', 'placeholder', 'E-mail/Gebruikersnaam')
        login.password().should('have.attr', 'placeholder', 'Wachtwoord')
        login.lostPasswordLink().should('contains.text', 'Wachtwoord vergeten?')
        login.loginButton().should('contains.text', 'Inloggen')
        login.supportLink().should('contains.text', 'Ondersteuning');        

    });

    it('When user click Bynder logo then go to the Bynder main page', () => {
        login.visitLoginPage()
        login.bynderLogoButton().click()
        cy.url().should('eq', 'https://www.bynder.com/en/')
    })

    it('Click Lost password? link and check the form and click Wave button', () => {
        login.lostPasswordLink().click();
        cy.url().should('eq', 'https://wave-trial.getbynder.com/user/forgotPassword/?redirectToken=')
        login.emailForgotPassword().should('exist')
        login.captchaForgotPassword().should('exist')
        login.captchaInputForgotPassword().should('exist')
        login.sendPasswordForgotLink().should('exist')
        login.waveLogoButton().click()
        cy.url().should('eq', 'https://wave-trial.getbynder.com/login/') 
    })

    it('Login changing Language to French and check button translations', () => {
        //When a user select -random- French language (as a sample)
        login.languageButton().click()
        login.languageList(2).eq(2, 'French (France)').click()
        login.email().type(Cypress.env('username'))
        login.password().type(Cypress.env('password'))
        login.loginButton().click()
        cy.url().should('eq', 'https://wave-trial.getbynder.com/account/dashboard/')
        login.mediaUploadButton()
                        .should('contain.text', 'Importer'); 
        login.profile().click()
        login.logoutButton()
                        .should('contain.text', 'Déconnexion'); 
        login.logoutButton().click()
        cy.url().should('eq', 'https://wave-trial.getbynder.com/login/')
        login.messageBox()
                        .should('have.text', 'Vous vous êtes correctement déconnecté.'); 
        login.languageButton().should('contains.text','Langue')
        login.email().should('have.attr', 'placeholder', 'E-mail/Identifiant')
        login.password().should('have.attr', 'placeholder', 'Mot de passe')
        login.lostPasswordLink().contains('Vous avez perdu votre mot de passe ?');
        login.loginButton().contains('Connexion');
        login.supportLink().contains('Assistance');
    })

    
    
});



