class login{
    email(email){
          cy.get('#inputEmail').type(email)
          return this
    }
    password(password){
          cy.get('#inputPassword').type(password)
          return this
    }
    loginBtn(){
          cy.get('.login-btn').click()
          return this
    }
    notificationIfExists(){
          const notifBtnClass = '.action-btn.blue.close'
          cy.get('body').then(($body) => {
               if ($body.find(notifBtnClass).length) {
               cy.get(notifBtnClass).click()
               }
               })      
          return this  
    }
    profile(){
          cy.get('.profile').click()
          return this
    }
    logoutBtn(){
          cy.get('form > .action-btn').click()
          return this
    }

    loginUrlAssertion(){
          cy.url().should('eq', 'https://wave-trial.getbynder.com/account/dashboard/')
          return this
    }

    logoutUrlAssertion(){
          cy.url().should('eq', 'https://wave-trial.getbynder.com/login/')
          return this
    }

    messageBoxAssert(text){
          cy.get('p.cbox_messagebox').should('have.text', text);
          return this
    }

    incorrectUsernameNotification(){
         cy.get('.notification>h1').should('have.text', 'You have entered an incorrect username or password.');
         return this
    }
    languageBtn(){
         cy.get('.admin-options>li>a').click()
         return this
    }
    languageBtnText(text){
          cy.get('.admin-options>li>a').contains(text);
          return this
    }
    languageOptions(){
         cy.get('a.admin-option').should(($lis) => {
          expect($lis).to.have.length(6)
          expect($lis.eq(0)).to.contain('Nederlands (Nederland)')
          expect($lis.eq(1)).to.contain('English (United States)')
          expect($lis.eq(2)).to.contain('Français (France)')
          expect($lis.eq(3)).to.contain('Deutsch (Deutschland)')
          expect($lis.eq(4)).to.contain('Italiano (Italia)')
          expect($lis.eq(5)).to.contain('Español (España)')
        })
         return this
    }
    selectLanguage(language){
          cy.get('a.admin-option').contains(language).click()
          return this
    }
    lostPasswordBtnText(text){
          cy.get('.lost-password > a').contains(text);
          return this
    }
    supportBtnText(text){
          cy.get('.text').contains(text);
          return this
    }
    emailBtnText(text){
          cy.get('#inputEmail').should('have.attr', 'placeholder', text)
          return this
    }
    passwordBtnText(text){
          cy.get('#inputPassword').should('have.attr', 'placeholder', text)
          return this
    }
    loginBtnText(text){
          cy.get('.login-btn').contains(text);
          return this
    }
    lostPasswordBtn(){
          cy.get('.lost-password>a').click()
          cy.url().should('eq', 'https://wave-trial.getbynder.com/user/forgotPassword/?redirectToken=')
          return this
    }
    bynderLogoBtn(){
          cy.get('.admin-bar-logo').click()
          return this
    }
    emailForgotPassword(){
          cy.get('#forgotPassword').should('exist')
          return this
    }
    captchaForgotPassword(){
          cy.get('.captcha').should('exist')
          return this
    }
    captchaInputForgotPassword(){
         cy.get('.required').should('exist')
         return this
    }
    sendPasswordForgot(){
         cy.get('#sendPassword').should('exist')
         return this
    }
    waveLogoBtn(){
         cy.get('.account-logo').click()
         cy.url().should('eq', 'https://wave-trial.getbynder.com/login/') 
         return this
    }
    mediaUploadBtn(text){
         cy.get('.cta-main').should('contain.text', text);
         return this
    }
    assertBynderUrl(){
         cy.url().should('eq', 'https://www.bynder.com/en/')
         return this
    }

}
export default login